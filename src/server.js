'use strict';

const PORT = 8080;
const HOST = '0.0.0.0';

var express = require('express');
var app = express();

var nodeadmin = require('nodeadmin');
app.use(nodeadmin(app));

app.get('/', (req, res) => {
    res.send(`<a href="/nodeadmin">Enter Node Admin</a>`);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);