#Set of the initial image
FROM node:alpine
#Defenition of the image description
LABEL Description="Angular 7 application"
#Defenition of the image author
LABEL Maintainer="yaroslav.belinsky@gmail.com"
# Create app directory
WORKDIR /app
#Defenition cursor in container
#ENV PS1='\[\033[1;32m\] \[\033[1;36m\][\u@\h] \[\033[1;34m\]\w\'
#copy of the package.json to the image
COPY package*.json ./
#run command inside container
RUN npm install
#copy sources to container
COPY src /app

EXPOSE 8080

#run entrypoint
CMD [ "node", "server.js" ]